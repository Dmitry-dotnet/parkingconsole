﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService, IDisposable
    {
        readonly ITimerService _withdrawTimer;
        readonly ITimerService _logTimer;
        readonly ILogService _logService;
        private Parking _parking;
        private bool _disposed;
        private List<TransactionInfo> _transactionInfo;
        private int _capacity
        {
            get { return Settings.ParkingCapacity; }
        }
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _transactionInfo = new List<TransactionInfo>();
            _parking = Parking.Instance;
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            LogTimerInit();
            WithdrawTimerInit();
        }

        public void Withdraw(Object source, ElapsedEventArgs e)
        {
            foreach (var car in _parking.Vehicles)
            {
                decimal tarif = Settings.tarif[car.VehicleType];
                if (car.Balance > 0 && car.Balance < tarif)
                {
                    decimal penalty = (tarif - car.Balance) * Settings.penaltyCoef;
                    decimal amount = (car.Balance + penalty);
                    _parking.Balance +=amount;
                    car.Balance = 0;
                    car.Balance -= penalty;
                    _transactionInfo.Add(new TransactionInfo(car, amount));
                }
                else if (car.Balance <= 0)
                {
                    decimal penalty = tarif * Settings.penaltyCoef;
                    _parking.Balance += penalty;
                    car.Balance -= penalty;
                    _transactionInfo.Add(new TransactionInfo(car, penalty));
                }
                else
                {
                    _parking.Balance += tarif;
                    car.Balance -= tarif;
                    _transactionInfo.Add(new TransactionInfo(car, tarif));
                }
            }
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactionInfo.ToArray();
        }
        public void  WriteInLog(Object source, ElapsedEventArgs e)
        {
            if (_transactionInfo.Count.Equals(0))
            {
                _logService.Write($"{DateTime.Now} - No Transaction Happends");
            }
            else
            {

                for (int i = 0; i < _transactionInfo.Count; i++)
                {
                    string transaction = _transactionInfo[i].transactionDate + " " + _transactionInfo[i].Sum + " " 
                        + "money withdrawn from vehicle with Id='"
                        +_transactionInfo[i].VehicleId + "'";
                    _logService.Write(transaction);
                }
                _transactionInfo = new List<TransactionInfo>();
            }
        }

        public void AddVehicle(JObject jObject)
        {
            var balanceType = jObject.Values().FirstOrDefault(x => x.Path.ToLower()
                                              .Equals("balance")).Type;
            if (balanceType is JTokenType.String)
            {
                throw new InvalidOperationException();
            }
            var type = typeof(Vehicle);
            try
            {
                string json = JsonConvert.SerializeObject(jObject);
                Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(json);
                AddVehicle(vehicle);
            }
            catch (Exception)
            {

                throw new InvalidOperationException();
            }

        }

        public void AddVehicle(Vehicle vehicle)
        {
            checkId(vehicle.Id);
            if (_parking.Vehicles.Any(v => v.Id == vehicle.Id))
            {

                throw new ArgumentException();
            }
            if (_parking.Vehicles.Count == Settings.ParkingCapacity)
            {
                throw new InvalidOperationException();
            }
            if (vehicle.VehicleType <= 0|| (int)vehicle.VehicleType > 4 )
            {
                throw new ArgumentException();
            }
            if (vehicle.Balance <= 0)
            {
                throw new ArgumentException();
            }
            else
            {
                _parking.Vehicles.Add(vehicle);
            }
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _capacity;
        }

        public int GetFreePlaces()
        {
            return Settings.ParkingCapacity - _parking.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.Vehicles);
        }

        public Vehicle GetVehicle(string id)
        {
            checkId(id);
            return GetVehicles().FirstOrDefault(x => x.Id == id);
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string id)
        {
            checkId(id);
            var car = _parking.Vehicles.FirstOrDefault(v => v.Id == id);
            if (car == null)
            {
                throw new ArgumentException();
            }
            if (car.Balance < 0)
            {
                throw new InvalidOperationException();
            }
            _parking.Vehicles.Remove(car);
        }

        public void TopUpVehicle(string id, decimal sum)
        {
            checkId(id);
            if (sum <= 0)
            {
                throw new InvalidOperationException();
            }
            var car = _parking.Vehicles.FirstOrDefault(v => v.Id == id);
            if (car != null)
            {
                car.Balance += sum;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public Vehicle TopUpVehicle(JObject jObject)
        {
            var sumType = jObject.Values().FirstOrDefault(x => x.Path.ToLower().Equals("sum")).Type;
            if (sumType is JTokenType.String)
            {
                throw new InvalidOperationException();
            }
            TopUpModel topUpModel;
            try
            {
                topUpModel = (TopUpModel)jObject.ToObject(typeof(TopUpModel));
                
            }
            catch (Exception)
            {
                throw new InvalidOperationException();
            }
            TopUpVehicle(topUpModel.id, topUpModel.Sum);
            return GetVehicle(topUpModel.id);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                _withdrawTimer.Elapsed -= Withdraw;
                _withdrawTimer.Dispose();
                _logTimer.Elapsed -= WriteInLog;
                _logTimer.Dispose();
                _transactionInfo.Clear();
                _parking.Vehicles.Clear();
                _parking.Balance = 0;

            }
                _disposed = true;
                
            
        }
        private void WithdrawTimerInit()
        {
            _withdrawTimer.Elapsed += Withdraw;
            _withdrawTimer.Interval = Settings.paymentPeriod;
            _withdrawTimer.Start();

        }

        private void LogTimerInit()
        {
            _logTimer.Elapsed += WriteInLog;
            _logTimer.Interval = Settings.logPeriod;
            _logTimer.Start();
        }

        private void checkId(string id)
        {
            if (!Regex.IsMatch(id, @"[A-Z][A-Z]-[0-9][0-9][0-9][0-9]-[A-Z][A-Z]"))
            {
                throw new InvalidOperationException("Id is invalid");
            }
        }

        ~ParkingService()
        {
            Dispose(false);
        }
    }
}