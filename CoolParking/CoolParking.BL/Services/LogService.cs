﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;


public class LogService : ILogService, IDisposable
{
    public string LogPath { get; set; }
    private StreamReader _reader { get; set; }
    public LogService(string logPath)
    {
        LogPath = logPath;
    }

    public string Read()
    {
        if (!File.Exists(LogPath))
        {
            throw new InvalidOperationException();
        }
        _reader = new StreamReader(LogPath);
        string info = _reader.ReadToEnd().ToString();
        _reader.Dispose();
        return info;
    }

    public virtual void Write(string logInfo)
    {
       
        using (StreamWriter sw = new StreamWriter(LogPath, true))
        {
            sw.WriteLine(logInfo);
            sw.Flush();
        }
    }

    public void Dispose()
    {
        _reader?.Dispose();
    }
}