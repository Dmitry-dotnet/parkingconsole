﻿using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService, IDisposable
    {
        private Timer _timer;
        private bool _disposed = false;
        public double Interval { get; set; }
        private event ElapsedEventHandler _elapsed;
        public event ElapsedEventHandler Elapsed
        {
            add { _timer.Elapsed += value;
                _elapsed += value;
            }
            remove { _timer.Elapsed -= value;
                _elapsed -= value;
            }
        }
        private delegate void Del();
        
        public void FireElapsedEvent()
        {
            var snapshot = _elapsed;
            _elapsed?.Invoke(this, null);
        }
        public TimerService()
        {
            _timer = new Timer();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Start()
        {
            _timer.Interval = this.Interval;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
            _disposed = true;
            }
        }

        ~TimerService()
        {
            Dispose(false);
        }
    }
}