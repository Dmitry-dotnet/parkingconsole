﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles = new List<Vehicle>();
        private static Parking _instance;
        
        private Parking()
        { }

        public static Parking Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Parking();
                }
                return _instance;
            }
        }
    }
}