﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{

    public static class Settings
    {
        public static double StartingBalance { get; set; }
        public static int ParkingCapacity { get; set; }
        public static double paymentPeriod { get; set; }
        public static double logPeriod { get; set; }
        public static Dictionary<VehicleType, decimal> tarif { get; set; }
        public static decimal penaltyCoef { get; set; }

        static Settings()
        {
            StartingBalance = 0;
            ParkingCapacity = 10;
            paymentPeriod = 5000;
            logPeriod = 30000;
            penaltyCoef = 2.5m;
            tarif = new Dictionary<VehicleType, decimal>() { {VehicleType.Bus, 3.5m },
                                                          { VehicleType.PassengerCar, 2},
                                                          { VehicleType.Truck, 5},
                                                          { VehicleType.Motorcycle, 1} };
        }
    }


}

