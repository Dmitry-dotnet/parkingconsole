﻿namespace CoolParking.BL.Models
{
    public class TopUpModel
    {
        public string id { get; set; }

        public decimal Sum { get; set; }

        public TopUpModel(string id, decimal sum)
        {
            this.id = id;
            Sum = sum;
        }
    }
}
