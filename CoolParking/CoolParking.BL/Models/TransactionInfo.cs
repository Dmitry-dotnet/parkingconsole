﻿using System;
namespace CoolParking.BL.Models {
    public struct TransactionInfo
    {
        public string VehicleId;
        public decimal Sum;
        public DateTime transactionDate;
        public TransactionInfo(Vehicle vehicle, decimal amount)
        {
            VehicleId = vehicle.Id;
            Sum = amount;
            transactionDate = DateTime.Now;
        }


    }
}