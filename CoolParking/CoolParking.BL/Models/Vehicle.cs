﻿using Newtonsoft.Json;
using System;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

public class Vehicle
{   
    [JsonPropertyName("id")]
    public string Id { get; }
    [JsonPropertyName("vehicleType")]
    public  VehicleType VehicleType { get; set; }
    [JsonPropertyName("balance")]
    public decimal Balance { get; internal set; }
    
    [JsonConstructor]
    public Vehicle(string id, VehicleType vehicleType, decimal balance)
    {
        Id = id;
        VehicleType = vehicleType;
        Balance = balance;
    }
    public Vehicle(VehicleType vehicleType, decimal balance)
    {
        if (balance < 0)
        {
            throw new System.ArgumentException();
        }
        Id = GenerateRandomRegistrationPlateNumber();
        VehicleType = vehicleType;
        Balance = balance;
    }


    public static string GenerateRandomRegistrationPlateNumber()
    {
        StringBuilder builder = new StringBuilder();
        builder.Append(generateRandomLetters());
        builder.Append("-");
        builder.Append(generateRandomNumbers());
        builder.Append("-");
        builder.Append(generateRandomLetters());

        return builder.ToString();

    }
    private static string generateRandomLetters()
    {
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        char ch;
        for (int i = 0; i < 2; i++)
        {
            ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
            builder.Append(ch);
        }
        return builder.ToString().ToUpper();
    }
    private static string generateRandomNumbers()
    {
        Random random = new Random();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 4; i++)
        {
            builder.Append( random.Next(0,9));
        }
        return builder.ToString();
    }

}