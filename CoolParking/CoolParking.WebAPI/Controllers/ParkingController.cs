﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
            
        }

        [HttpGet]
        [Route("[action]")]
        public ActionResult<decimal> Balance()
        {
            return Ok(_parkingService.GetBalance());
        }

        [HttpGet]
        [Route("[action]")]
        public ActionResult<int> Capacity()
        {
            return Ok(_parkingService.GetCapacity());
        }

        [HttpGet]
        [Route("[action]")]
        public ActionResult<int> FreePlaces()
        {
            return Ok(_parkingService.GetFreePlaces());
        }
    }
}