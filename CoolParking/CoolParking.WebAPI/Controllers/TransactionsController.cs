﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        [Route("[action]")]
        public ActionResult<TransactionInfo[]> Last()
        {
            return _parkingService.GetLastParkingTransactions();
        }

        [HttpGet]
        [Route("[action]")]
        public ActionResult<string> All()
        {
            try
            {
                return _parkingService.ReadFromLog();
            }
            catch (InvalidOperationException)
            {
                return NotFound("Log file not found");
            }
        }

        [HttpPut]
        [Route("[action]")]
        public ActionResult<Vehicle> TopUpVehicle(JObject jObject)
        {
            try
            {
                return _parkingService.TopUpVehicle(jObject);
            }
            catch(InvalidOperationException)
            {
                return BadRequest("Body is invalid");
            }
            catch (ArgumentException)
            {
                return NotFound("Vehicle not found");
            }
        }


    }
}