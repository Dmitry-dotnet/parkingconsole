﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        [Route("")]
        public ActionResult<IReadOnlyCollection<Vehicle>> Vehicles()
        {
            return _parkingService.GetVehicles();
        }
        
        [HttpGet]
        [Route("{id}")]
        public ActionResult<Vehicle> Vehicles(string id)
        {
            try
            {
                if (_parkingService.GetVehicle(id) == null)
                {
                    return NotFound("Vehicle with this id is not exist");
                }
                return _parkingService.GetVehicle(id);
            }
            catch (InvalidOperationException)
            {
                return BadRequest("Wrong format of ID");
            }
           
        }

        [HttpPost]
        [Route("")]
        public IActionResult AddVehicle(JObject jObject)
        {
            try
            {
                _parkingService.AddVehicle(jObject);
                return StatusCode(201, jObject);
            }
            catch(Exception)
            {
                return BadRequest("Body is invalid");
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult RemoveVeicle(string id)
         {
            try
            {
            _parkingService.RemoveVehicle(id);
            }
            catch (InvalidOperationException)
            {
                return BadRequest("Id is invalid");
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}