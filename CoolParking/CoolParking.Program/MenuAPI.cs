﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking.Interface
{
    public class MenuAPI : Menu
    {
        private readonly HttpClient _httpClient;
        

        public MenuAPI()
        {
            _httpClient = new HttpClient();
        }

        public override void MainMenu()
        {

            Console.Clear();
            Console.WriteLine("Welcome to a Parking!");
            Console.WriteLine("Please chose an action");
            Console.WriteLine(" 1 - Watch current Balance of parking");
            Console.WriteLine(" 2 - Watch parking Capacity");
            Console.WriteLine(" 3 - Watch free places");
            Console.WriteLine(" 4 - Watch vehicles");
            Console.WriteLine(" 5 - Watch vehicle by id");
            Console.WriteLine(" 6 - Add Vehicle");
            Console.WriteLine(" 7 - Delete Vehicle");
            Console.WriteLine(" 8 - Watch last transactions before logging");
            Console.WriteLine(" 9 - Read from log");
            Console.WriteLine("10 - Top Up Vehicle");
            Console.WriteLine(" 0 - Exit");
            int value = checkInput();
            switch (value)
            {
                case 1:
                    watchBalance().Wait();
                    break;
                case 2:
                    watchCapacity().Wait();
                    break;
                case 3:
                    watchFreePlaces().Wait();
                    break;
                case 4:
                    watchVehicles().Wait();
                    break;
                case 5:
                    try
                    {
                        watchVehicleById().Wait();
                    }
                    catch (Exception)
                    {
                        Error("Not found");
                        Console.ReadKey();
                        MainMenu();
                    }
                    break;
                case 6:
                    try
                    {
                        addVehicle().Wait();
                    }
                    catch(Exception)
                    {
                        Error("Wrong data input");
                        Console.ReadKey();
                        MainMenu();
                    }
                    break;
                case 7:
                    try
                    {
                        deleteVehicle().Wait();
                    }
                    catch (Exception)
                    {
                        Error("Not found");
                        Console.ReadKey();
                        MainMenu();
                    }
                    break;
                case 8:
                    lastTransactios().Wait();
                    break;
                case 9:
                    allTransacton().Wait();
                    break;
                case 10:
                    topUpVehicle().Wait();
                    break;
                case 0:
                    break;
                default:
                    MainMenu();
                    break;
            }

        }

        private async Task watchBalance()
        {
            
            var content = await _httpClient.GetStringAsync("https://localhost:44326/api/parking/balance");
            var responseMessage = await _httpClient.GetAsync("https://localhost:44326/api/parking/balance");
            ColorStatus(responseMessage);
            ShowContent(content);
            MainMenu();
            return;
        }

        private async Task watchCapacity()
        {

            var content = await _httpClient.GetStringAsync("https://localhost:44326/api/parking/capacity");
            var responseMessage = await _httpClient.GetAsync("https://localhost:44326/api/parking/capacity");
            ColorStatus(responseMessage);
            ShowContent(content);
            MainMenu(); 
            return;
        }
        private async Task watchFreePlaces()
        {

            var content = await _httpClient.GetStringAsync("https://localhost:44326/api/parking/freeplaces");
            var responseMessage = await _httpClient.GetAsync("https://localhost:44326/api/parking/freeplaces");
            ColorStatus(responseMessage);
            ShowContent(content);
            MainMenu();
            return;
        }

        private async Task watchVehicles()
        {

            var content = await _httpClient.GetStringAsync("https://localhost:44326/api/vehicles");
            var responseMessage = await _httpClient.GetAsync("https://localhost:44326/api/vehicles");
            ColorStatus(responseMessage);
            ShowContent(content);
            MainMenu();
            return;
        }

        private async Task watchVehicleById()
        {
            
            Warning("Insert Serial Number of Vehicle (ID) in format AA-1111-AA");
            string id = Console.ReadLine();
            if (!Regex.IsMatch(id, @"[A-Z][A-Z]-[0-9][0-9][0-9][0-9]-[A-Z][A-Z]"))
            {
                Error("Wrong format of id, ytpe any key and try again");
                Console.ReadKey();
                await watchVehicleById();
            }

                var content = await _httpClient.GetStringAsync($"https://localhost:44326/api/vehicles/{id}");
                var responseMessage = await _httpClient.GetAsync($"https://localhost:44326/api/vehicles/{id}");
            ColorStatus(responseMessage);
            ShowContent(content);
            Console.ReadKey();
            MainMenu();
            return;
        }

        private async Task addVehicle()
        {
            Console.WriteLine("Choose type of your car: \n" +
                "1 - PassengerCar \n" +
                "2 - Truck \n" +
                "3 - Bus \n" +
                "4 - Motorcycle \n" +
                "\n");
            Warning("5 - go to main menu");

            int carType = checkInput();
            if ((carType > 4 || carType < 1) && carType != 5)
            {
                Error("Choose existing type, type any key and try again");
                Console.Beep();
                Console.ReadKey();
                await addVehicle();
                return;
            }
            else if (carType == 5)
            {
                MainMenu();
                return;
            }
            VehicleType type = (VehicleType)carType;
            Console.WriteLine("Insert Balance amount (more 0)");
            int balance = checkInput();
            Console.WriteLine("Insert Serial Number of Vehicle (ID) in format AA-1111-AA or type [G]" +
                "\n to generate random Id");
            string id = Console.ReadLine().ToLower();
            if (id.Equals("g"))
            {
                try
                {
                    Vehicle vehicleNew = new Vehicle(type, balance);
                    string json = JsonConvert.SerializeObject(vehicleNew);

                    var data = new StringContent(json, Encoding.Default, "application/json");
                    var responseMessage = await _httpClient.PostAsync($"https://localhost:44326/api/vehicles/", data);
                    ColorStatus(responseMessage);
                    Console.ReadKey();
                    MainMenu();
                    return;
                }
                catch (ArgumentException)
                {
                    Error("Incorrect Balance, type any key to try again");
                    Console.Beep();
                    Console.ReadKey();
                    await addVehicle();
                }
            }
            else
            {

                try
                {
                    id = id.ToUpper();
                    Vehicle vehicleNew = new Vehicle(id, type, balance);
                    string json = JsonConvert.SerializeObject(vehicleNew);
                    var data = new StringContent(json, Encoding.Default, "application/json");
                    var responseMessage = await _httpClient.PostAsync($"https://localhost:44326/api/vehicles/", data);
                    ColorStatus(responseMessage);
                    Console.ReadKey();
                    MainMenu();
                    return;
                }
                catch (ArgumentException)
                {
                    Error("Incorrect ID or Balance type any key to try again");
                    Console.Beep();
                    Console.ReadKey();
                    await addVehicle();
                }
            }
        }

        private async Task deleteVehicle()
        {
            Warning("Insert Serial Number of Vehicle (ID) in format AA-1111-AA");
            string id = Console.ReadLine();
            if (!Regex.IsMatch(id, @"[A-Z][A-Z]-[0-9][0-9][0-9][0-9]-[A-Z][A-Z]"))
            {
                Error("Wrong format of id, ytpe any key and try again");
                Console.ReadKey();
                await deleteVehicle();
            }

            var responseMessage = await _httpClient.DeleteAsync($"https://localhost:44326/api/vehicles/{id}");
            MainMenu();
            return;
        }

        private async Task lastTransactios()
        {
            Console.Clear();
            var content = await _httpClient.GetStringAsync("https://localhost:44326/api/transactions/last");
            var responseMessage = await _httpClient.GetAsync("https://localhost:44326/api/transactions/last");
            ColorStatus(responseMessage);
            ShowContent(content);
            MainMenu();
            return;
        }

        private async Task allTransacton()
        {
            Console.Clear();
            var content = await _httpClient.GetStringAsync("https://localhost:44326/api/transactions/all");
            var responseMessage = await _httpClient.GetAsync("https://localhost:44326/api/transactions/all");
            ColorStatus(responseMessage);
            ShowContent(content);
            MainMenu();
            return;
        }

        private async Task topUpVehicle()
        {
            Warning("Insert Serial Number of Vehicle (ID) in format AA-1111-AA");
            string id = Console.ReadLine();
            if (!Regex.IsMatch(id, @"[A-Z][A-Z]-[0-9][0-9][0-9][0-9]-[A-Z][A-Z]"))
            {
                Error("Wrong format of id, ytpe any key and try again");
                Console.ReadKey();
                await topUpVehicle();
            }
            Console.WriteLine("Insert amount you want to pop up");
            decimal sum = checkInputDecimal();

            TopUpModel topUpModel = new TopUpModel(id, sum);
            string json = JsonConvert.SerializeObject(topUpModel);

            var data = new StringContent(json, Encoding.Default, "application/json");
            var responseMessage = await _httpClient.PutAsync($"https://localhost:44326/api/transactions/TopUpVehicle", data);
            ColorStatus(responseMessage);
            Console.ReadKey();
            MainMenu();
            return;


        }

        private void ColorStatus(HttpResponseMessage responseMessage)
        {
            if (responseMessage.IsSuccessStatusCode)
            {
                Success($"Status: {responseMessage.StatusCode}");
                
            }
            else
            {
                Error($"Status: {responseMessage.StatusCode}");
            }
        }

        private void ShowContent(string result)
        {
            Console.WriteLine($"Content: \n{result}");
            Console.ReadKey();
            Console.WriteLine("type any key to go back to main menu");
        }

        private decimal checkInputDecimal()
        {
            string input = Console.ReadLine();
            decimal result;
            if (!decimal.TryParse(input, out result))
            {
                Error("Insert decimal value");
                return checkInput();
            }
            return result;
        }
    }
}
