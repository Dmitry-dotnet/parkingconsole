﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.IO;
using System.Reflection;

namespace CoolParking.Interface
{   
    //This class remains after preious version of Project
    public class Menu
    {
        private IParkingService _parkingServise;
        readonly ITimerService _withdrawTimer;
        readonly ITimerService _logTimer;
        readonly ILogService _logService;

        public Menu()
        {

                _withdrawTimer = new TimerService();
                _logTimer = new TimerService();
                _logService = new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\transactions.txt");
                _parkingServise = new ParkingService(_withdrawTimer, _logTimer, _logService);
            
        }
        public virtual void MainMenu()
        {

            Console.Clear();
            Console.WriteLine("Welcome to a Parking!");
            Console.WriteLine("Please, choose an action");
            Console.WriteLine("1 - Watch balance of parking");
            Console.WriteLine("2 - Show total amount of cash before logging");
            Console.WriteLine("3 - Watch Capacity and Free Places");
            Console.WriteLine("4 - Show transaction story before logging");
            Console.WriteLine("5 - Show transaction story from log");
            Console.WriteLine("6 - Watch Vehicles");
            Console.WriteLine("7 - Add Vehicle");
            Console.WriteLine("8 - Delete Vehicle");
            Console.WriteLine("9 - TopUp balance of vehicle");
            Console.WriteLine("10 - Exit");
            int value = checkInput();
            switch (value)
            {
                case 1:
                    watchBalance();
                    break;
                case 2:
                    cashBeforeLogging();
                    break;
                case 3:
                    freePlaces();
                    break;
                case 4:
                    storyBeforeLogging();
                    break;
                case 5:
                    readLog();
                    break;
                case 6:
                    watchVehicles();
                    break;
                case 7:
                    addVehicle();
                    break;
                case 8:
                    deleteVehicle();
                    break;
                case 9:
                    topUpVehicle();
                    break;
                case 10:
                    break;
                default:
                    MainMenu();
                    break;

            }
        }

        private void storyBeforeLogging()
        {
            if (_parkingServise.GetLastParkingTransactions().Length != 0)
            {
                foreach (var transaction in _parkingServise.GetLastParkingTransactions())
                {
                    Console.WriteLine(transaction.transactionDate + " " + transaction.VehicleId + " " + "SUM: " + transaction.Sum);
                }
                Console.ReadKey();
                MainMenu();
                return;
            }
            else
            {
                Warning("There are no transactions yet");
                Console.ReadKey();
                MainMenu();
                return;
            }
        }
        private void readLog()
        {
            Console.WriteLine(_parkingServise.ReadFromLog());
            Console.ReadKey();
            MainMenu();
            return;
        }
        private void freePlaces()
        {
            Console.WriteLine($"Current Capacity is {Settings.ParkingCapacity}");
            Console.WriteLine($"Places available: {_parkingServise.GetFreePlaces()}");
            Warning("Press any key to go back");
            Console.ReadKey();
            MainMenu();
            return;
        }
        
        private void cashBeforeLogging()
        {
            Console.Clear();
            decimal sum = 0;
            foreach (var item in _parkingServise.GetLastParkingTransactions())
            {
                sum += item.Sum;
            }
            Success($"Total amount before logging equals {sum}");
            Console.ReadKey();
            MainMenu();
            return;
        }

        private void addVehicle()
        {
            Console.Clear();
            if (_parkingServise.GetFreePlaces().Equals(0))
            {
                Error("There are no free places available.");
                Console.ReadKey();
                MainMenu();
                return;
            }
            Console.WriteLine("Choose type of your car: \n" +
                "1 - PassengerCar \n" +
                "2 - Truck \n" +
                "3 - Bus \n" +
                "4 - Motorcycle \n" +
                "\n");
            Warning("5 - go to main menu");

            int carType = checkInput();
            if ((carType > 4 || carType < 1) && carType != 5)
            {
                Error("Choose existing type, type any key and try again");
                Console.Beep();
                Console.ReadKey();
                addVehicle();
                return;
            }
            else if (carType == 5)
            {
                MainMenu();
                return;
            }
            VehicleType type = (VehicleType)carType;
            Console.WriteLine("Insert Balance amount");
            int balance = checkInput();
            Console.WriteLine("Insert Serial Number of Vehicle (ID) in format AA-1111-AA or type [G]" +
                "\n to generate random Id");
            string id = Console.ReadLine().ToLower();
            if (id.Equals("g"))
            {
                try
                {
                    Vehicle vehicleNew = new Vehicle(type, balance);
                    _parkingServise.AddVehicle(vehicleNew);
                    Success("Vehicle added successfully");
                    Console.ReadKey();
                    MainMenu();
                    return;
                }
                catch (ArgumentException)
                {
                    Error("Incorrect Balance, type any key to try again");
                    Console.Beep();
                    Console.ReadKey();
                    addVehicle();
                }

            }
            else
            {

                try
                {
                    Vehicle vehicleNew = new Vehicle(id, type, balance);
                    _parkingServise.AddVehicle(vehicleNew);
                    Success("Vehicle added successfully");
                    Console.ReadKey();
                    MainMenu();
                    return;
                }
                catch (ArgumentException)
                {
                    Error("Incorrect ID or Balance type any key to try again");
                    Console.Beep();
                    Console.ReadKey();
                    addVehicle();
                }
            }
           
        }

        private void watchVehicles()
        {
            ShowVehicles();
            Warning("Press any key to go back to menu");
            Console.ReadLine();
            MainMenu();
            return;
        }

        private void deleteVehicle()
        {
            Console.Clear();
            ShowVehicles();
            Warning("Insert ID of vehicle you want to delete or type 0 to exit");
            string id = Console.ReadLine();
            if (id.Equals("0"))
            {
                MainMenu();
                return;
            }
            try
            {
                _parkingServise.RemoveVehicle(id);
                Success("Vehicle removed successfully");
                Console.ReadKey();
                MainMenu();
                return;
            }
            catch (ArgumentException)
            {
                Error("Wrong ID, try again");
                deleteVehicle();
                return;
            }
        }
        private void ShowVehicles()
        {
            if (_parkingServise.GetVehicles().Count != 0)
            {
                Console.WriteLine("Current vehicles");

                foreach (var vehicle in _parkingServise.GetVehicles())
                {

                    Console.WriteLine("====================");
                    Console.WriteLine($"|| ID      - {vehicle.Id} \n|| TYPE    - {vehicle.VehicleType} " +
                        $"\n|| BALANCE - {vehicle.Balance}");
                    Console.WriteLine("====================");
                }
            }
            else
            {
                Warning("There is no vehicles on parking right now");
            }
        }

        private void watchBalance()
        {
            Console.Clear();
            Warning("Current balance of parking is " + _parkingServise.GetBalance());
            Console.ReadKey();
            MainMenu();
            return;
        }

        private void topUpVehicle()
        {
            Console.Clear();
            ShowVehicles();
            Warning("Insert id of vehicle you want to top up or type 0 to go back");
            string id = Console.ReadLine();
            if (id.Equals("0"))
            {
                MainMenu();
                return;
            }
            Warning("Insert amount of balance");
            decimal amount = checkInput();
            try
            {
                _parkingServise.TopUpVehicle(id, amount);
                Success("Vehicle balance increased successfully");
                Console.ReadKey();
                MainMenu();
                return;
            }
            catch (ArgumentException)
            {
                Error("Wrong ID or balance, try again");
                topUpVehicle();
                return;
            }
        }


        private protected int checkInput()
        {
            string input = Console.ReadLine();
            int result;
            if (!int.TryParse(input, out result))
            {
                Error("Insert int value");
                return checkInput();
            }
            return result;
        }

        private protected void Error(string text)
        {
            Console.Beep();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }

        private protected void Warning(string text)
        {   
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }

        private protected void Success(string text)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}

